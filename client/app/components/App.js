import React, { Component } from 'react';
import axios from 'axios';

class App extends Component {
	constructor(props, context) {
		super(props, context);
		this.state = {
		  knowledgeBases: {}
		};
	  }
	
	componentDidMount() {
    
	
	 axios.get('http://199.127.62.234:2000/api/knowledgebase')
      .then(res => {
        this.setState({ knowledgeBases: res.data[0]['Categories'][1] });
		console.log("res",res);
      })
      .catch((error) => {
		  console.log("error",error);
      }); 
  }
  
  render() {
    return (
      <div className="App">
        <p className="App-intro">
          In Client Component App.js file
		  {this.state.knowledgeBases.name}
        </p>
      </div>
    );
  }
}

export default App;
