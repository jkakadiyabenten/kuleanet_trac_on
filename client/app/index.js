import React from 'react';
import ReactDOM from 'react-dom';
import { Router, hashHistory } from 'react-router';
import routes from './config/routes';
import jquery from 'jquery';

import './../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './../../node_modules/font-awesome/css/font-awesome.css'


ReactDOM.render(
    <Router history={hashHistory}>{routes}</Router>,
    document.getElementById('root')
);