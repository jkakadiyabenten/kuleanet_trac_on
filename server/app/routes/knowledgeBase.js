var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var knowledgeBase =require('../models/knowledgeBase');

/* GET ALL RECORDS */
router.get('/', function(req, res) {
	console.log("res in routes in knowledgebase");
  knowledgeBase.find(function (err, knowledgebases) {
      if (err) return next(err);
      res.json(knowledgebases);
	  console.log("response in routes in knowledgebase",knowledgebases);
    });
});

module.exports = router;
