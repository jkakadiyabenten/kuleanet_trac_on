var mongoose = require('mongoose');

var knowledgeBaseSchema = new mongoose.Schema({
    Categories:[
    {
    name: String,
    url: String,
    icon: String,
    order: String,
    subcategories: [
        {
            title: String,
            detailed_info: String,
            suburl:String,
            references: String,
            image: String,
            video: String
        }]    
}]});


module.exports = mongoose.model('knowledgeBase', knowledgeBaseSchema);