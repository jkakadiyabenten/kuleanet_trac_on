const express = require('express');
const bodyParser = require('body-parser');
var port = process.env.PORT || 2000;

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
//mongodb://199.127.62.234:2915/kuleanet
mongoose.connect('mongodb://199.127.62.234:2915/kuleanet', { promiseLibrary: require('bluebird') })
  .then(() =>  console.log('connection successful'))
  .catch((err) => console.error(err));
  
var app = express();

var knowledgeBase = require('./server/app/routes/knowledgeBase');

app.use(express.static('./server/static/'));
app.use(express.static('./client/public/'));

app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
        next();
    });
	
// set up our express application
app.use(bodyParser.urlencoded({ extended: false })); // get information from html forms
app.use(bodyParser.json())
app.use('/api/knowledgeBase', knowledgeBase);

app.set('view engine', 'ejs'); // set up ejs for templating

// launch ======================================================================
app.listen(port, () => {
	console.log('The magic happens on port ' + port);
});
